<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comentarios;
use AppBundle\Entity\Notificacoes;
use AppBundle\Entity\Transacoes;
use Doctrine\ORM\ORMException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\Tests\Compiler\J;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @return JsonResponse
     */
    public function indexAction(Request $request)
    {
        return new JsonResponse(
            [
                'status' => 'ok',
                'mensagem' => 'API para envio de comentários e postagens - Renan Caride - 2018'
            ]
        );
    }

    /**
     * Postar comentário: Endpoint para o usuário adicionar comentário a uma postagem
     *
     * @Route("/comentar", name="Postar Comentário")
     * @Method({"POST"})
     * @return JsonResponse
     *
     * Parametros: postagem, usuario, comentario, creditos_destaque
     * */
    public function comentarAction(Request $request) {

        // Repositorios
        $comentariosRep = $this->getDoctrine()->getRepository('AppBundle:Comentarios');

        // Valida os parâmetros enviados
        if ($comentariosRep->validaComentar($request->request->all())) {
            $usuario = $this->getDoctrine()->getRepository('AppBundle:Usuarios')
                ->find($request->get('usuario'));
            $postagem = $this->getDoctrine()->getRepository('AppBundle:Postagens')
                ->find($request->get('postagem'));
            if ($usuario && $postagem) { // Usuario e postagem existem?
                /* Regras para comentário */
                // 1. Comprando destaque
                if ($usuario->getComprandoDestaque() == 'y') {
                    $valorDescontar = $request->get('creditos_destaque') + ($request->get('creditos_destaque') * ($this->container->getParameter('porcentagem_destaque') / 100));
                    if ($valorDescontar > $usuario->getCreditos()) { // Não possui créditos suficientes
                        return new JsonResponse(
                            [
                                'status' => 'erro',
                                'mensagem' => 'Usuário não possui creditos para comprar destaque'
                            ]
                        );
                    }
                // 2. Usuarios não assinantes
                } else {
                    $usuarioPostagem = $this->getDoctrine()->getRepository('AppBundle:Usuarios')
                        ->find($postagem->getIdUsuario());
                    if (($usuarioPostagem->getAssinante() == 'n') && ($usuario->getAssinante() == 'n')) {
                        return new JsonResponse(
                            [
                                'status' => 'erro',
                                'mensagem' => 'Não assinante não comenta em postagem de não assinante'
                            ]
                        );
                    }
                }
                // 3. Está dentro do limite de tempo
                $limiteComentarios = $this->container->getParameter('limite_comentarios');
                $limiteTempo = $this->container->getParameter('limite_segundos');
                if ($comentariosRep->validaQtdeComentarios($limiteComentarios, $limiteTempo, $request->get('usuario'))) {
                    // Comenta
                    try {
                        $manager = $this->getDoctrine()->getManager();

                        // Entidade para persistencia
                        $comentario = new Comentarios();
                        $comentario->setIdPostagem($postagem);
                        $comentario->setIdUsuario($usuario);
                        $comentario->setDestaque($usuario->getComprandoDestaque());
                        $comentario->setCreditosDestaque($request->get('creditos_destaque'));
                        $comentario->setComentario($request->get('comentario'));
                        $comentario->setDataHora(new \DateTime('now', new \DateTimeZone('America/Sao_Paulo')));

                        // Persistindo
                        $manager->persist($comentario);
                        $manager->flush();

                        /* Transação e Notificação */
                        if ($usuario->getComprandoDestaque() == 'y') {
                            // Gravar transação do usuário
                            $transacao = new Transacoes();
                            $transacao->setIdUsuario($usuario);
                            $transacao->setHistorico('Comprando destaque na postagem '.$postagem->getId());
                            $transacao->setValor($request->get('creditos_destaque'));
                            $transacao->setDataHora(new \DateTime('now', new \DateTimeZone('America/Sao_Paulo')));
                            $manager->persist($transacao);
                            $manager->flush();

                            // Transação do sistema
                            $valorSistema = $request->get('creditos_destaque') * ($this->container->getParameter('porcentagem_destaque')/100);
                            $transacao = new Transacoes();
                            $transacao->setIdUsuario($usuario);
                            $transacao->setHistorico('Valor retido do destaque da postagem '.$postagem->getId());
                            $transacao->setValor($valorSistema);
                            $transacao->setDataHora(new \DateTime('now', new \DateTimeZone('America/Sao_Paulo')));
                            $manager->persist($transacao);
                            $manager->flush();

                            // Descontar os creditos do usuário
                            $usuario->setCreditos($usuario->getCreditos() - $valorDescontar);
                            $manager->persist($usuario);
                            $manager->flush();
                        }

                        // Notificação ao dono da postagem
                        $notificacao = new Notificacoes();
                        $notificacao->setIdDestinatario($postagem->getIdUsuario());
                        $notificacao->setNotificacao('O usuário '.$usuario->getLogin().' comentou em sua postagem '.$postagem->getId());
                        $notificacao->setDataHora(new \DateTime('now', new \DateTimeZone('America/Sao_Paulo')));
                        $manager->persist($notificacao);
                        $manager->flush();

                        // Enviando Email
                        $destinatario = $this->getDoctrine()->getRepository('AppBundle:Usuarios')
                            ->find($postagem->getIdUsuario());
                        if ($destinatario->getEmail()) {
                            $mailer = new \Swift_Mailer();
                            $mensagem = (new \Swift_Message('Nova notificação!'))
                                ->setFrom('apirenan@comentariosrenan.com')
                                ->setTo($destinatario->getEmail())
                                ->setBody(
                                    $this->renderView(
                                        'Emails/notificacao.html.twig',
                                        [
                                            'login' => $destinatario->getLogin(),
                                            'usuario' => $usuario->getLogin(),
                                            'postagem' => $postagem->getId()
                                        ]
                                    ),
                                    'text/html'
                                );
                            $mailer->send($mensagem);
                        }

                        // Resposta
                        return new JsonResponse(
                            [
                                'status' => 'ok',
                                'mensagem' => 'Comentário postado!'
                            ]
                        );
                    } catch (ORMException $e) {
                        return new JsonResponse(
                            [
                                'status' => 'erro',
                                'mensagem' => $e->getMessage()
                            ]
                        );
                    }
                }

                // Usuário excedeu o número de comentários
                return new JsonResponse(
                    [
                        'status' => 'erro',
                        'mensagem' => 'Usuário excedeu o limite de '.$limiteComentarios.' a cada '.$limiteTempo.' segundos'
                    ]
                );

            }

            // Não existe o usuário ou postagem
            return new JsonResponse(
                [
                    'status' => 'erro',
                    'mensagem' => 'Verificar usuário e postagem enviados'
                ]
            );
        }

        // Não validou
        return new JsonResponse(
            [
                'status' => 'erro',
                'mensagem' => 'Não foram enviados todos os campos para comentar na postagem'
            ],
            500
        );

    }

    /**
     * Excluir comentário: Endpoint para o usuário excluir um comentário
     *
     * @Route("/excluir", name="Excluir Comentário")
     * @Method({"POST"})
     * @return JsonResponse
     *
     * Parametros: usuario, comentario_excluir, usuario_excluir, postagem_excluir
     * */
    public function excluirAction(Request $request) {
        if ($request->get('usuario') &&
            (($request->get('comentario_excluir')) || ($request->get('usuario_excluir')) || ($request->get('postagem_excluir')))) {
            // Usuário efetuando a exlcusão
            $usuario = $this->getDoctrine()->getRepository('AppBundle:Usuarios')
                ->find($request->get('usuario'));
            if ($usuario) {
                $manager = $this->getDoctrine()->getManager();
                /* Testes para exclusão do comentário */
                // Objeto do comentário
                if ($request->get('comentario_excluir')) {
                    $comentario = $this->getDoctrine()->getRepository('AppBundle:Comentarios')
                        ->find($request->get('comentario_excluir'));
                    // 1. Excluir comentário: ou é dono da postagem ou é dono do comentário
                    if ($comentario) {

                        $excluiu = false;
                        if ($usuario == $comentario->getIdUsuario()) { // Dono do comentário, exclui
                            $manager->remove($comentario);
                            $manager->flush();
                            $excluiu = true;
                        } else { // Verificar se é o dono da postagem
                            $postagem = $this->getDoctrine()->getRepository('AppBundle:Postagens')
                                ->find($comentario->getIdPostagem());
                            if ($postagem && ($postagem->getIdUsuario() == $usuario)) { // Dono da postagem, exclui
                                $manager->remove($comentario);
                                $manager->flush();
                                $excluiu = true;
                            }
                        }

                        if ($excluiu) {
                            return new JsonResponse(
                                [
                                    'status' => 'ok',
                                    'mensagem' => 'Comentário excluído'
                                ]
                            );
                        }
                    }
                }
                // 2. Excluir comentários de um usuário na postagem: somente dono da postagem
                if ($request->get('usuario_excluir') && $request->get('postagem_excluir')) {
                    $postagem = $this->getDoctrine()->getRepository('AppBundle:Postagens')
                        ->find($request->get('postagem_excluir'));
                    if (($postagem) && ($usuario == $postagem->getIdUsuario())) { // É o dono da postagem
                        if ($comentarios = $this->getDoctrine()->getRepository('AppBundle:Comentarios')
                                ->findBy([
                                    'idUsuario' => $request->get('usuario_excluir'),
                                    'idPostagem' => $request->get('postagem_excluir')
                                ])) { // Comentários do usuário na postagem
                            foreach ($comentarios as $comentario) {
                                $manager->remove($comentario);
                                $manager->flush();
                            }

                            return new JsonResponse(
                                [
                                    'status' => 'ok',
                                    'mensagem' => 'Comentários do usuário '.$request->get('usuario_excluir').' excluídos'
                                ]
                            );
                        }
                    }
                }
            }
        }

        return new JsonResponse(
            [
                'status' => 'erro',
                'mensagem' => 'Verificar campos enviados'
            ],
            500
        );
    }

    /**
     * Listar Notificações: Endpoint para listar as notificações do usuário
     *
     * @Route("/notificacoes", name="Listar Notificações")
     * @Method({"POST"})
     * @return JsonResponse
     *
     * Parametros: usuario
     * */
    public function notificacoesAction(Request $request) {
        if ($request->get('usuario')) {
            // Consulta das notificações
            $notificacoes = $this->getDoctrine()->getRepository('AppBundle:Notificacoes')
                ->listaNotificacoes($request->get('usuario'), $this->getParameter('validade_notificacao'));
            if (count($notificacoes) > 0) {
                $dados = [];
                $cont = 0;
                foreach ($notificacoes as $notificacao) {
                    $dados[$cont]["notificacao"] = $notificacao->getNotificacao();
                    $dados[$cont]["hora"] = date_format($notificacao->getDataHora(), 'H:i');
                    $cont++;
                }

                return new JsonResponse(
                    [
                        'status' => 'ok',
                        'notificacoes' => $dados
                    ]
                );
            }

            return new JsonResponse(
                [
                    'status' => 'ok',
                    'mensagem' => 'O usuário não possui notificações'
                ]
            );
        }

        return new JsonResponse(
            [
                'status' => 'erro',
                'mensagem' => 'Fornecer o usuario para listar as notificações'
            ],
            500
        );
    }

    /**
     * Listar Comentários: Endpoint para listar os comentários por usuário ou postagem
     *
     * @Route("/comentarios", name="Listar Comentários")
     * @Method({"GET"})
     * @return JsonResponse
     *
     * Parametros: usuario, postagem, pagina
     * */
    public function comentariosAction(Request $request) {
        if ($request->get('usuario') || $request->get('postagem')) {
            // Página vem como parâmetro ou é padrão 1
            $pagina = ($request->get('pagina') ? $request->get('pagina') : 1);
            if (!is_numeric($pagina))
                $pagina = 1;

            // Comentarios (sem considerar compra destaque)
            $comentarios = $this->getDoctrine()->getRepository('AppBundle:Comentarios')
                ->listaComentarios($request->get('usuario'), $request->get('postagem'), $pagina);

            /* Gerar informações para retorno */
            // 1. Primeiro separa destaque dos normais
            $normais = [];
            $destaque = [];
            $comentariosArr = [];
            if (count($comentarios) > 1) {
                foreach ($comentarios as $comentario) {
                    $limiteInf = new \DateTime('now', new \DateTimeZone('America/Sao_Paulo'));
                    $limiteInf->modify('- '.$comentario->getCreditosDestaque().' minutes');
                    $limiteInf = $limiteInf->getTimestamp(); // Timestamp limite

                    $dataHora = strtotime(date_format($comentario->getDataHora(), 'Y-m-d H:i:s')); // Timestamp do comentario

                    if ($dataHora > $limiteInf)
                        $destaque[] = $comentario;
                    else
                        $normais[] = $comentario;
                }

                // Coloca ambos os comentários em um só array
                if (count($destaque) > 0) {
                    foreach ($destaque as $cada_destaque) {
                        $comentariosArr[] = $cada_destaque;
                    }
                }
                foreach ($normais as $cada_normal) {
                    $comentariosArr[] = $cada_normal;
                }
            } elseif (count($comentarios) == 1) {
                $comentariosArr[] = $comentario;
            }
            // 2. Cria a resposta com as informações necessárias
            if (count($comentariosArr) > 0) {
                $dados = [];
                $cont = 0;
                foreach ($comentariosArr as $cada_comentario) {
                    $usuario = $cada_comentario->getIdUsuario();
                    $dados[$cont]['id_usuario'] = $usuario->getId();
                    $dados[$cont]['id_comentario'] = $cada_comentario->getId();
                    $dados[$cont]['login'] = $usuario->getLogin();
                    $dados[$cont]['assinante'] = $usuario->getAssinante();
                    $dados[$cont]['destaque'] = $cada_comentario->getDestaque();
                    $dados[$cont]['data_hora'] = date_format($cada_comentario->getDataHora(), 'd/m/Y - H:i:s');
                    $dados[$cont]['comentario'] = $cada_comentario->getComentario();
                    $cont++;
                }

                // Retorna os comentários
                /*return new JsonResponse(
                    [
                        'status' => 'ok',
                        'comentarios' => $dados
                    ]
                );*/

                // Cache no retorno
                $resposta = new JsonResponse(
                    [
                        'status' => 'ok',
                        'comentarios' => $dados
                    ]
                );
                $resposta->setPublic();
                $resposta->setExpires(new \DateTime("+5 minutes"));
                $resposta->setLastModified(new \DateTime("-5 minutes"));
                $resposta->headers->addCacheControlDirective('must-revalidate', true);

                return $resposta;
            }

            // Não possui resultados
            return new JsonResponse(
                [
                    'status' => 'ok',
                    'mensagem' => 'Não foram encontrados comentários com esses parâmetros'
                ]
            );

        }

        return new JsonResponse(
            [
                'status' => 'erro',
                'mensagem' => 'Enviar usuário ou postagem'
            ],
            500
        );
    }
}
