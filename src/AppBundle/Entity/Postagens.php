<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Postagens
 *
 * @ORM\Table(name="postagens", indexes={@ORM\Index(name="fk_postagens_usuarios", columns={"id_usuario"})})
 * @ORM\Entity
 */
class Postagens
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="texto_postagem", type="text", nullable=true)
     */
    private $textoPostagem;

    /**
     * @var string
     *
     * @ORM\Column(name="imagem_postagem", type="string", length=255, nullable=true)
     */
    private $imagemPostagem;

    /**
     * @var string
     *
     * @ORM\Column(name="video_postagem", type="string", length=255, nullable=true)
     */
    private $videoPostagem;

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuario", referencedColumnName="id")
     * })
     */
    private $idUsuario;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTextoPostagem()
    {
        return $this->textoPostagem;
    }

    /**
     * @param string $textoPostagem
     */
    public function setTextoPostagem($textoPostagem)
    {
        $this->textoPostagem = $textoPostagem;
    }

    /**
     * @return string
     */
    public function getImagemPostagem()
    {
        return $this->imagemPostagem;
    }

    /**
     * @param string $imagemPostagem
     */
    public function setImagemPostagem($imagemPostagem)
    {
        $this->imagemPostagem = $imagemPostagem;
    }

    /**
     * @return string
     */
    public function getVideoPostagem()
    {
        return $this->videoPostagem;
    }

    /**
     * @param string $videoPostagem
     */
    public function setVideoPostagem($videoPostagem)
    {
        $this->videoPostagem = $videoPostagem;
    }

    /**
     * @return \Usuarios
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }

    /**
     * @param \Usuarios $idUsuario
     */
    public function setIdUsuario($idUsuario)
    {
        $this->idUsuario = $idUsuario;
    }
}

