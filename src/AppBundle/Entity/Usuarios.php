<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Usuarios
 *
 * @ORM\Table(name="usuarios")
 * @ORM\Entity
 */
class Usuarios
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", length=50, nullable=false)
     */
    private $login;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="assinante", type="string", length=1, nullable=false)
     */
    private $assinante = 'n';

    /**
     * @var string
     *
     * @ORM\Column(name="comprando_destaque", type="string", length=1, nullable=false)
     */
    private $comprandoDestaque = 'n';

    /**
     * @var float
     *
     * @ORM\Column(name="creditos", type="float", precision=9, scale=2, nullable=false)
     */
    private $creditos = '0.00';

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getAssinante()
    {
        return $this->assinante;
    }

    /**
     * @param string $assinante
     */
    public function setAssinante($assinante)
    {
        $this->assinante = $assinante;
    }

    /**
     * @return string
     */
    public function getComprandoDestaque()
    {
        return $this->comprandoDestaque;
    }

    /**
     * @param string $comprandoDestaque
     */
    public function setComprandoDestaque($comprandoDestaque)
    {
        $this->comprandoDestaque = $comprandoDestaque;
    }

    /**
     * @return float
     */
    public function getCreditos()
    {
        return $this->creditos;
    }

    /**
     * @param float $creditos
     */
    public function setCreditos($creditos)
    {
        $this->creditos = $creditos;
    }
}

