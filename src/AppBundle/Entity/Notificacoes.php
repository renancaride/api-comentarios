<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Notificacoes
 *
 * @ORM\Table(name="notificacoes", indexes={@ORM\Index(name="fk_notificacoes_usuarios", columns={"id_destinatario"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NotificacoesRepository")
 */
class Notificacoes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="notificacao", type="string", length=255, nullable=false)
     */
    private $notificacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_hora", type="datetime", nullable=false)
     */
    private $dataHora;

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_destinatario", referencedColumnName="id")
     * })
     */
    private $idDestinatario;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNotificacao()
    {
        return $this->notificacao;
    }

    /**
     * @param string $notificacao
     */
    public function setNotificacao($notificacao)
    {
        $this->notificacao = $notificacao;
    }

    /**
     * @return \DateTime
     */
    public function getDataHora()
    {
        return $this->dataHora;
    }

    /**
     * @param \DateTime $dataHora
     */
    public function setDataHora($dataHora)
    {
        $this->dataHora = $dataHora;
    }

    /**
     * @return \Usuarios
     */
    public function getIdDestinatario()
    {
        return $this->idDestinatario;
    }

    /**
     * @param \Usuarios $idDestinatario
     */
    public function setIdDestinatario($idDestinatario)
    {
        $this->idDestinatario = $idDestinatario;
    }
}

