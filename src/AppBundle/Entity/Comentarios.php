<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Comentarios
 *
 * @ORM\Table(name="comentarios", indexes={@ORM\Index(name="fk_comentarios_postagens", columns={"id_postagem"}), @ORM\Index(name="fk_comentarios_usuarios", columns={"id_usuario"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ComentariosRepository")
 */
class Comentarios
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="destaque", type="string", length=1, nullable=false)
     */
    private $destaque = 'n';

    /**
     * @var float
     *
     * @ORM\Column(name="creditos_destaque", type="float", precision=9, scale=2, nullable=false)
     */
    private $creditosDestaque = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="comentario", type="text", length=65535, nullable=false)
     */
    private $comentario;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_hora", type="datetime", nullable=false)
     */
    private $dataHora;

    /**
     * @var \Postagens
     *
     * @ORM\ManyToOne(targetEntity="Postagens")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_postagem", referencedColumnName="id")
     * })
     */
    private $idPostagem;

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuario", referencedColumnName="id")
     * })
     */
    private $idUsuario;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getDestaque()
    {
        return $this->destaque;
    }

    /**
     * @param string $destaque
     */
    public function setDestaque($destaque)
    {
        $this->destaque = $destaque;
    }

    /**
     * @return float
     */
    public function getCreditosDestaque()
    {
        return $this->creditosDestaque;
    }

    /**
     * @param float $creditosDestaque
     */
    public function setCreditosDestaque($creditosDestaque)
    {
        $this->creditosDestaque = $creditosDestaque;
    }

    /**
     * @return string
     */
    public function getComentario()
    {
        return $this->comentario;
    }

    /**
     * @param string $comentario
     */
    public function setComentario($comentario)
    {
        $this->comentario = $comentario;
    }

    /**
     * @return \DateTime
     */
    public function getDataHora()
    {
        return $this->dataHora;
    }

    /**
     * @param \DateTime $dataHora
     */
    public function setDataHora($dataHora)
    {
        $this->dataHora = $dataHora;
    }

    /**
     * @return \Postagens
     */
    public function getIdPostagem()
    {
        return $this->idPostagem;
    }

    /**
     * @param \Postagens $idPostagem
     */
    public function setIdPostagem($idPostagem)
    {
        $this->idPostagem = $idPostagem;
    }

    /**
     * @return \Usuarios
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }

    /**
     * @param \Usuarios $idUsuario
     */
    public function setIdUsuario($idUsuario)
    {
        $this->idUsuario = $idUsuario;
    }
}

