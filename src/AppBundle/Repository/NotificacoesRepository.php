<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class NotificacoesRepository extends EntityRepository
{

    /*
     * Listar as notificações do usuário
     * */
    public function listaNotificacoes($usuario = false, $tempoLimite = false) {
        if ($usuario && $tempoLimite) {
            // Limite inferior do momento do post
            $limiteInf = new \DateTime('now', new \DateTimeZone('America/Sao_Paulo'));
            $limiteInf->modify('- '.$tempoLimite.' hours');
            $limiteInf = date_format($limiteInf, 'Y-m-d H:i:s');

            // Query: notificações dentro do período
            return $this->getEntityManager()
                ->createQuery(
                    "SELECT n
                          FROM AppBundle:Notificacoes n 
                          WHERE n.dataHora > '".$limiteInf."'
                           AND n.idDestinatario = $usuario
                          ORDER BY n.dataHora DESC"
                )
                ->getResult();
        }

        return [];
    }
}