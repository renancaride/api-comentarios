<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class ComentariosRepository extends EntityRepository
{
    /*
     * Método que valida os campos enviados na requisição
     * */
    public function validaComentar($parametros = []) {
        if (count($parametros) > 0) {
            // Campos obrigatórios para comentário
            $camposObrigatorios = ['postagem', 'usuario', 'comentario'];
            $camposEnviados = array_keys($parametros);

            if (count(array_diff($camposObrigatorios, $camposEnviados)) == 0) // Possui todos os campos
                return true;
        }

        return false; // Não passou na validação
    }

    /*
     * Método para validar se o usuário está dentro do limite de comentários
     * */
    public function validaQtdeComentarios($qtde = false, $tempo = false, $usuario = false) {
        if ($qtde && $tempo) {
            // Limite inferior do momento do post
            $limiteInf = new \DateTime('now', new \DateTimeZone('America/Sao_Paulo'));
            $limiteSup = new \DateTime('now', new \DateTimeZone('America/Sao_Paulo'));
            $limiteInf->modify('- '.$tempo.' seconds');
            $limiteInf = date_format($limiteInf, 'Y-m-d H:i:s');
            $limiteSup = date_format($limiteSup, 'Y-m-d H:i:s');

            // Query: quantidade de comentários dentro do periodo
            $resultado = $this->getEntityManager()
                ->createQuery(
                    "SELECT COUNT(c)
                          FROM AppBundle:Comentarios c 
                          WHERE c.dataHora BETWEEN '".$limiteInf."' AND '".$limiteSup."'
                           AND c.idUsuario = $usuario"
                )
                ->getSingleResult();
            // Dentro do limite
            if ($resultado[1] <= $qtde)
                return true;
        }

        return false;
    }

    /*
     * Método para listar os comentários, ordenado por data e creditos postagem, com paginação
     * */
    public function listaComentarios($usuario = false, $postagem = false, $pagina = 1) {
        if ($usuario || $postagem) {
            // Todos os comentários
            $qb = $this->createQueryBuilder('c');

            if ($usuario) {
                $qb->where('c.idUsuario = :usuario')->setParameter('usuario', $usuario);
                if ($postagem)
                    $qb->andWhere('c.idPostagem = :postagem')->setParameter('postagem', $postagem);
            } elseif($postagem) {
                $qb->where('c.idPostagem = :postagem')->setParameter('postagem', $postagem);
            }

            $qb->orderBy('c.dataHora', 'DESC');

            // Definindo primeiro resultado
            $offset = 0;
            if ($pagina > 1) {
                $offset = 20 * ($pagina - 1);
            }

            $qb->setMaxResults(20)->setFirstResult($offset);

            return $qb->getQuery()->getResult();
        }

        return [];
    }
}