api-comentarios
===============

API com Endpoints para:

1. Comentar em uma postagem:<br>
**URL:** /comentar<br>
**Método:** POST<br> 
**Parâmetros:** postagem, usuario, comentario, creditos_destaque
2. Excluir um comentário:<br>
**URL:** /excluir<br>
**Método:** POST<br> 
**Parâmetros:**
    <ul>
    <li>usuario: Usuário que efetua a requisição
    <li>comentario_excluir: id do comentário a se excluir
    <li>usuario_excluir: excuir comentários do usuário (somente dono da postagem)
    <li>postagem_excluir: excuir comentários da postagem (somente dono da postagem)
    </ul>
3. Listar notificações de um usuário:<br>
**URL:** /notificacoes<br>
**Método:** POST<br> 
**Parâmetros:** usuario
4. Listar comentários da postagem ou do usuário:<br>
**URL:** /comentarios<br>
**Método:** GET<br> 
**Parâmetros:** usuario, postagem, pagina

